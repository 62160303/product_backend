const express = require('express')
const router = express.Router()
const User = require('../models/User')
// const users = [
//   { id: 1, name: 'Ipad gen1 64G Wifi', price: 10000.0 },
//   { id: 2, name: 'Ipad gen2 64G Wifi', price: 11000.0 },
//   { id: 3, name: 'Ipad gen3 64G Wifi', price: 12000.0 },
//   { id: 4, name: 'Ipad gen4 64G Wifi', price: 13000.0 },
//   { id: 5, name: 'Ipad gen5 64G Wifi', price: 14000.0 },
//   { id: 6, name: 'Ipad gen6 64G Wifi', price: 15000.0 },
//   { id: 7, name: 'Ipad gen7 64G Wifi', price: 16000.0 },
//   { id: 8, name: 'Ipad gen8 64G Wifi', price: 17000.0 },
//   { id: 9, name: 'Ipad gen9 64G Wifi', price: 18000.0 },
//   { id: 10, name: 'Ipad gen10 64G Wifi', price: 19000.0 }
// ]
// const lastId = 11

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User Not Found !'
      })
    }
    res.status(200).json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const addUsers = async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const updateUsers = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteUsers = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findOneAndDelete(userId)
    res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

router.get('/', getUsers) // Get All Users
router.get('/:id', getUser)// Get Specifed User
router.post('/', addUsers)// Add New User
router.put('/:id', updateUsers)// Add New User
router.delete('/:id', deleteUsers)

module.exports = router
